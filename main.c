#include <ncurses.h>
#include <string.h>

#define WELCOME_MSG "Welcome to LANchat!"

#define AS_SERVER 0
#define AS_CLIENT 1

int main (int argc, char *argv[])
{
    int scrMaxY, scrMaxX;
    WINDOW *titleWin;

    WINDOW *menuWin;
    char *menuOptions[2] = {"Create new chat room", "Join existing chat room"};
    int menuSelection = 0;
    int i, keyCode;

    // Initialize the main screen and hides the cursor
    initscr();
    noecho();
    curs_set(0);

    // Get the main screen's dimensions and create the title window
    getmaxyx(stdscr, scrMaxY, scrMaxX);
    titleWin = newwin(5, scrMaxX - 2, 1, 1);

    // Draw the title window
    wmove(titleWin, 2, (scrMaxX - 2 - strlen(WELCOME_MSG)) / 2);
    attron(A_BOLD);
    box(titleWin, 0, 0);
    wprintw(titleWin, WELCOME_MSG);
    attroff(A_BOLD);
    wrefresh(titleWin);

    // Create the menu window
    menuWin = newwin(4, scrMaxX, 7, 1);
    keypad(menuWin, 1);

    // Draw the menu
    wprintw(menuWin, "Please, select an option:");
    while (1) {
        for (i = 0; i < 2; i++)
        {
            wmove(menuWin, i + 2, 5);

            if (menuSelection == i)
                wprintw(menuWin, "* %s", menuOptions[i]);
            else
                wprintw(menuWin, "  %s", menuOptions[i]);
        }

        wrefresh(menuWin);

        keyCode = wgetch(menuWin);
        if (keyCode == KEY_UP && menuSelection == 1)
            menuSelection = 0;
        else if (keyCode == KEY_DOWN && menuSelection == 0)
            menuSelection = 1;
        else if (keyCode == '\n')
            break;
    }

    switch (menuSelection)
    {
        case AS_SERVER:
            break;
        case AS_CLIENT:
    }

    // Destroy all windows from memory
    delwin(menuWin);
    delwin(titleWin);
    endwin();

    return 0;
}
